import axios from 'axios';

const URL = 'https://test-1563974580527.firebaseio.com/test.json';

export const request = async (method, data = null) => {

    let body = '';

    if (data) {
        body = {
            method: method,
            url: URL,
            data: data
        };
    } else {
        body = {
            method: method,
            url: URL
        };
    }

    try {
        const response = await axios(body);

        if (response) {
            return response.data;
        }

    } catch (err) {
        console.log(err);
    }
}
