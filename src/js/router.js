export const router = (state) => {

    const urlParams = new URLSearchParams(window.location.search);

    const x = urlParams.get('x');
    const y = urlParams.get('y');
    const value = urlParams.get('value');

    let route = window.location.pathname;

    if (x && y && value) {
        route = '/restoreState';
    }

    switch (route) {
        case '/':
            state.getTable();
            break;

        case '/restoreState':
            state.restoreState(x, y, value);
            break;

        case '/initial':
            state.intialTable(); // initial database
            break;
    }
};