import {
  LoremIpsum
} from "lorem-ipsum";
import {
  request
} from './api';

export default class Table {
  constructor() {
    this.table = null;
    this.current = null;
    this.app = document.getElementById('app');
  }

  async intialTable() {
    const lorem = new LoremIpsum({
      wordsPerSentence: {
        min: 6,
        max: 8
      }
    });

    const arr = new Array(40).fill().map(() => (new Array(5).fill().map(() => (lorem.generateSentences(1)))));

    this.table = await request('put', JSON.stringify(arr));
  }

  async getTable() {
    const result = await request('get');

    if (result) {
      this.table = result;
      this.isMobileView() ? this.generateMobileTable() : this.generateTable();
    }
  }

  isMobileView() {
    return window.innerWidth <= 760;
  }

  async updateTable() {

    await request('put', JSON.stringify(this.table));

    while (this.app.firstChild) {
      this.app.removeChild(this.app.firstChild);
    }

    this.getTable()
  }

  async restoreState(x, y, value) {

    const result = await request('get');

    if (result) {
      this.table = result;
      this.table[y][x] = value;

      const isMobile = this.isMobileView();

      isMobile ? this.generateMobileTable() : this.generateTable();

      this.popup('restore', x, y, value);
    }
  }

  generateTable() {
    let tableDOM = '';

    this.table.map((row, y) => {

      if (y) {
        tableDOM += '<div class="row"><div class="add-row">&plus;</div><div class="remove-row">&times;</div>';
      } else {
        tableDOM += '<div class="row">';
      }

      row.map((col, x) => {
        if (y) {
          tableDOM += `<div class="col" data-col="${x}" data-row="${y}">${col}</div>`;
        } else {
          tableDOM += `<div class="col th" data-col="${x}" data-row="${y}">${col}
          <div class="add-col">&plus;</div><div class="remove-col">&times;</div></div>`;
        }
      });

      tableDOM += '</div>';
    });

    this.app.insertAdjacentHTML('beforeend', `<div class="table">${tableDOM}</div>`);
    this.initPopup();
    this.addListeners();
  }

  generateMobileTable() {
    let tableDOM = '';
    let i = false;

    this.table.map((row, y) => {
      if (y) {
        tableDOM += '<div class="row"><div class="add-row">&plus;</div><div class="remove-row">&times;</div>';

        row.map((col, x) => {
          if (i) {
            tableDOM += `<div class="mth" data-col="${x}" data-row="0">${this.table[0][x]}</div><div class="col" data-col="${x}" data-row="${y}">${col}</div>`;
          } else {
            tableDOM += `<div class="mth" data-col="${x}" data-row="0">${this.table[0][x]}<div class="add-col">&plus;</div><div class="remove-col">&times;</div></div><div class="col" data-col="${x}" data-row="${y}">${col}</div>`;
          }
        });

        i = true;

        tableDOM += '</div>';
      }
    });

    this.app.insertAdjacentHTML('beforeend', `<div class="table">${tableDOM}</div>`);
    this.initPopup();
    this.addListeners();
  }

  initPopup() {
    this.app.insertAdjacentHTML('beforeend', '<div class="popup"><div class="popup-content"></div></div>');
  }

  addListeners() {
    const state = this;
    let prevCellState = '';

    const popup = document.querySelector(".popup");
    const cells = document.querySelectorAll('.col, .mth');

    const getCurrentRow = (e) => e.target.parentNode.children[3].dataset.row;
    const getCurrentCol = (e) => e.target.parentNode.dataset.col;

    cells.forEach(item => item.addEventListener('click', e => {
      e.target.setAttribute('contenteditable', 'true');
      e.target.focus();
      prevCellState = e.target.textContent;
      history.pushState({}, '', `/`)
    }));

    cells.forEach(item => item.addEventListener('blur', e => {
      e.target.removeAttribute('contenteditable');

      const x = e.target.dataset.col;
      const y = e.target.dataset.row;
      const value = e.target.textContent;

      if (prevCellState !== value) {
        history.replaceState({}, '', `?x=${x}&y=${y}&value=${value}`);
        state.popup('edit', x, y, value);
      }
    }));

    cells.forEach(item => item.addEventListener('keydown', e => {
      if (e.keyCode === 13) {
        e.preventDefault();
        e.target.removeAttribute('contenteditable');
      }
    }));

    const addListener = (className, isRow) => document.querySelectorAll(`.${className}`).forEach(item => item.addEventListener('click', e => {
      if (isRow) {
        const currentRow = getCurrentRow(e);
        this.popup(className, null, currentRow);
      } else {
        const currentCol = getCurrentCol(e);
        this.popup(className, currentCol);
      }
    }));

    addListener('add-row', true);
    addListener('remove-row', true);
    addListener('add-col', false);
    addListener('remove-col', false);

    document.addEventListener('keydown', e => {
      if (e.keyCode === 27) {
        popup.classList.remove('show-popup');
      }
    });

    popup.addEventListener('click', e => {
      const lorem = new LoremIpsum({
        wordsPerSentence: {
          min: 6,
          max: 8
        }
      });

      const target = e.target.classList.value;
      const rowId = e.target.dataset.rowid;
      const colId = e.target.dataset.colId;

      const close = () => {
        popup.classList.toggle('show-popup');
        document.querySelector('body').classList.toggle('fixed');
      }

      switch (target) {
        case 'popup-content_close':
          close();
          break;

        case 'edit-cell_button':
          this.updateTable();
          close();

          break;

        case 'add-row_button':
          const columns = this.table[rowId].length;
          const arr = new Array(columns).fill().map(() => (lorem.generateSentences(1)));
          this.table.splice(rowId - 1, 0, arr);
          this.updateTable();
          close();

          break;
        case 'remove-row_button':
          this.table.splice(rowId, 1);
          this.updateTable();
          close();

          break;

        case 'add-col_button':
          this.table.map(item => item.splice(colId - 1, 0, lorem.generateSentences(1)));
          this.updateTable();
          close();

          break;
        case 'remove-col_button':
          this.table.map(item => item.splice(colId, 1));
          this.updateTable();
          close();

          break;
        case 'popup show-popup':
          close();

          break;
      }
    });
  }

  popup(mode, x = null, y = null, value = null) {

    const popup = document.querySelector(".popup");
    popup.classList.toggle("show-popup");

    const insertPopupContent = (body) => popup.children[0].innerHTML = body;

    const fixed = () => document.querySelector("body").classList.toggle('fixed');

    const elem = document.querySelector(`[data-col="${x}"][data-row="${y}"]`);

    switch (mode) {

      case 'edit':
        insertPopupContent(`<div class="popup-content_close">&times;</div><div class="popup-content_header">Are you sure to save data?</div>
        <div class="popup-content_body">Cell x=${x} y=${y} to new text. <p>${value}</p></div>
        <div class="popup-content_footer">
        <button class="edit-cell_button">Save</button></div>`);
        fixed();
        break;

      case 'restore':

        insertPopupContent(`<div class="popup-content_close">&times;</div><div class="popup-content_header">Are you sure to save data?</div>
        <div class="popup-content_body">Cell x=${x} y=${y} to new text. <p>${value}</p></div>
        <div class="popup-content_footer">
        <button class="edit-cell_button">Save</button></div>`);

        document.querySelector("body").style.top = `-${elem.getBoundingClientRect().top}px`;

        fixed();
        break;

      case 'add-row':
        insertPopupContent(`<div class="popup-content_close">&times;</div><div class="popup-content_header">Are you sure to add new row?</div>
        <div class="popup-content_body">Current row is ${y}.</div>
        <div class="popup-content_footer"><button class="add-row_button" data-rowid=${y}>Add row</button></div>`);
        fixed();
        break;

      case 'remove-row':
        insertPopupContent(`<div class="popup-content_close">&times;</div><div class="popup-content_header">Are you sure to delete row?</div>
      <div class="popup-content_body">Current row is ${y}.</div><div class="popup-content_footer"><button class="remove-row" data-rowid=${y}>Delete row</button></div>`);
        fixed();
        break;

      case 'add-col':
        insertPopupContent(`<div class="popup-content_close">&times;</div><div class="popup-content_header">Are you sure to add new column?</div>
        <div class="popup-content_body">Current column is ${x}.</div>
        <div class="popup-content_footer"><button class="add-col_button" data-colid=${x}>Add column</button></div>`);
        fixed();
        break;

      case 'remove-col':
        insertPopupContent(`<div class="popup-content_close">&times;</div><div class="popup-content_header">Are you sure to delete column?</div>
      <div class="popup-content_body">Current column is ${x}.</div><div class="popup-content_footer"><button class="remove-col_button" data-colid=${x}>Delete column</button></div>`);
        fixed();
        break;

      default:
        break;
    }
  }
}