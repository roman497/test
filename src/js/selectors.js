const selectorDOM = (node, all) => all ? document.querySelectorAll(node) : document.querySelector(node);

export const POPUP = selectorDOM('.popup', false);
export const NOTE_ALL = selectorDOM('.node', true);
export const NOTES = selectorDOM('.notes', false);