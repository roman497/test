import Table from './table';
import {
    router
} from './router';
import '../css/style.css';

const state = {};

state.table = new Table;

router(state.table);
